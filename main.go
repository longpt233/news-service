package main

import (
	"article-service/internal/infrastructure/router"
	"fmt"
	"log"

	"github.com/joho/godotenv"
)

func loadEnv() {
	if err := godotenv.Load(".env.dev"); err != nil {
		log.Println("Error loading .env file")
	} else {
		log.Println("Load .env successfully")
	}
}

func main() {
	fmt.Println("Main Application Starts")
	loadEnv()
	router.RunAPI()

}
