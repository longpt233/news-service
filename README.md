# init project

```
$ go mod init new-service
go: creating new go.mod: module new-service
$ go run main.go 
Hello World!
```

# go install

```
go get -u github.com/gin-gonic/gin
go get github.com/segmentio/kafka-go
```


# autoscale 

https://www.densify.com/kubernetes-autoscaling/kubernetes-hpa



```
long@hello:~/Documents/20221/wrk$ ./wrk --timeout 30s -t12 -c100 -d300s http://20.24.127.183/news/api/v1/articles?filter=id.2022110922431027
Running 5m test @ http://20.24.127.183/news/api/v1/articles?filter=id.2022110922431027
  12 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     6.48s     5.42s   29.98s    85.32%
    Req/Sec     1.95      2.95    30.00     83.76%
  3658 requests in 5.00m, 133.91MB read
  Socket errors: connect 0, read 0, write 0, timeout 138
Requests/sec:     12.19
Transfer/sec:    456.96KB
```

```

le [ ~ ]$ kubectl -n default autoscale deployment news-deployment-meta-name --cpu-percent=50 --min=5 --max=10
horizontalpodautoscaler.autoscaling/news-deployment-meta-name autoscaled
le [ ~ ]$ kubectl get hpa 
NAME                        REFERENCE                              TARGETS   MINPODS   MAXPODS   REPLICAS   AGE
news-deployment-meta-name   Deployment/news-deployment-meta-name   2%/50%    5         10        1          4m45s
le [ ~ ]$ kubectl top pod       
NAME                                          CPU(cores)   MEMORY(bytes)           
news-deployment-meta-name-57bdbbb5c7-wjn48    29m          20Mi  
news-deployment-meta-name-57bdbbb5c7-7b6wp    22m          20Mi                           
news-deployment-meta-name-57bdbbb5c7-nh8gc    22m          17Mi    
news-deployment-meta-name-57bdbbb5c7-q56fm    22m          13M
news-deployment-meta-name-57bdbbb5c7-z6bq9    22m          14Mi 


long@hello:~/Documents/20221/wrk$ ./wrk --timeout 30s -t12 -c100 -d300s http://20.24.127.183/news/api/v1/articles?filter=id.2022110922431027
Running 5m test @ http://20.24.127.183/news/api/v1/articles?filter=id.2022110922431027
  12 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.50s     1.19s   26.18s    92.18%
    Req/Sec     7.96      5.43    40.00     78.29%
  21060 requests in 5.00m, 770.98MB read
Requests/sec:     70.18
Transfer/sec:      2.57MB

le [ ~ ]$ kubectl get hpa 
NAME                        REFERENCE                              TARGETS   MINPODS   MAXPODS   REPLICAS   AGE
news-deployment-meta-name   Deployment/news-deployment-meta-name   54%/50%   5         10        7          11m 

le [ ~ ]$ kubectl top pod
NAME                                          CPU(cores)   MEMORY(bytes)                                                                     
news-deployment-meta-name-57bdbbb5c7-7b6wp    22m          20Mi                           
news-deployment-meta-name-57bdbbb5c7-nh8gc    22m          17Mi    
news-deployment-meta-name-57bdbbb5c7-q56fm    22m          13Mi    
news-deployment-meta-name-57bdbbb5c7-wjn48    23m          16Mi    
news-deployment-meta-name-57bdbbb5c7-xclxm    22m          12Mi     
news-deployment-meta-name-57bdbbb5c7-z6bq9    22m          14Mi 
news-deployment-meta-name-57bdbbb5c7-f967z    22m          14Mi

```