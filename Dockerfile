FROM golang:1.19-alpine

WORKDIR /app

COPY go.mod .
COPY go.sum .

RUN go mod tidy

COPY . .

EXPOSE 8091

RUN go build -o ./run main.go

CMD ["./run"]