# k8s 

### cách connect tới 1 cluster 

![](./asset/init-agent.png)

### trên azuze  

```
phan [ ~ ]$ az aks get-credentials --resource-group datn-group --name news-cluster
phan [ ~ ]$ helm repo add gitlab https://charts.gitlab.io"gitlab" has been added to your repositories
phan [ ~ ]$ helm repo updateHang tight while we grab the latest from your chart repositories......Successfully got an update from the "gitlab" chart repository
Update Complete. ⎈Happy Helming!⎈phan [ ~ ]$ helm upgrade --install news-cluster-agent gitlab/gitlab-agent \
    --namespace gitlab-agent \    --create-namespace \
    --set image.tag=v15.4.0 \
    --set config.token=**** \    --set config.kasAddress=wss://kas.gitlab.com
Release "news-cluster-agent" does not exist. Installing it now.
NAME: news-cluster-agentLAST DEPLOYED: Wed Aug 31 07:52:46 2022NAMESPACE: gitlab-agentSTATUS: deployedREVISION: 1TEST SUITE: None
```

- sau khi connect xong, sẽ cuất hiện thêm ns của mình

```
phan [ ~ ]$ kubectl get deployments --all-namespaces=true
NAMESPACE      NAME                              READY   UP-TO-DATE   AVAILABLE   AGE
gitlab-agent   news-cluster-agent-gitlab-agent   1/1     1            1           115s
kube-system    coredns                           2/2     2            2           21h
kube-system    coredns-autoscaler                1/1     1            1           21h
kube-system    konnectivity-agent                2/2     2            2           21h
kube-system    metrics-server                    2/2     2            2           21h
```

- trên ui gitlab 

![](./asset/sucess.png)
# deployment

- update zezo down time. [doc](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/)
- khi mình tạo một service/loadBalancer thì trên ui tương ứng cx tạo cho mình một cái loadbalance

![](./asset/loadBalancer.png)

```
phan [ ~ ]$ kubectl get all -o wide
NAME                                  READY   STATUS             RESTARTS   AGE   IP            NODE                                NOMINATED NODE   READINESS GATES
pod/news-meta-name-5fd6556654-mjkhl   1/1     Running            0          14m   10.244.0.13   aks-agentpool-30258697-vmss000000   <none>           <none>
pod/news-meta-name-f65f964bf-76kj7    0/1     ImagePullBackOff   0          11m   10.244.0.14   aks-agentpool-30258697-vmss000000   <none>           <none>

NAME                   TYPE           CLUSTER-IP     EXTERNAL-IP    PORT(S)          AGE   SELECTOR
service/kubernetes     ClusterIP      10.0.0.1       <none>         443/TCP          22h   <none>
service/lb-meta-name   LoadBalancer   10.0.155.215   20.121.153.3   2025:31001/TCP   26m   app=news-labels

NAME                             READY   UP-TO-DATE   AVAILABLE   AGE   CONTAINERS            IMAGES                                  SELECTOR
deployment.apps/news-meta-name   1/1     1            1           26m   news-container-name   longpt233/news-service-image:3ff911c3   app=news-labels

NAME                                        DESIRED   CURRENT   READY   AGE   CONTAINERS            IMAGES                                  SELECTOR
replicaset.apps/news-meta-name-5fd6556654   1         1         1       14m   news-container-name   longpt233/news-service-image:latest     app=news-labels,pod-template-hash=5fd6556654
replicaset.apps/news-meta-name-6f96d4b5df   0         0         0       26m   news-container-name   longpt233/news-service-image:latest     app=news-labels,pod-template-hash=6f96d4b5df
replicaset.apps/news-meta-name-f65f964bf    1         1         0       11m   news-container-name   longpt233/news-service-image:3ff911c3   app=news-labels,pod-template-hash=f65f964bf

```