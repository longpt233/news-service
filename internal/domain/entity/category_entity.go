package entity

import "gorm.io/gorm"

// has-many relationship: https://gorm.io/docs/has_many.html
// NOT belong to

type CategoryEntity struct {
	gorm.Model
	Name            string
	ArticleEntities []ArticleEntity
}

// func (CategoryEntity) TableName() string { return "category_tb" }
