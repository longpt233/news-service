package entity

type ArticleEntity struct {
	Model
	Title            string `json:"title" gorm:"type:varchar(3000)"`
	Content          string `json:"content" gorm:"type:longtext"`
	Img              string `gorm:"type:varchar(3000)"`
	Raw              string `gorm:"type:longtext"`
	CategoryEntityId uint   `json:"category_entity_id"` // unint because gorm.Model ID is uint
}

// func (ArticleEntity) TableName() string { return "article_tb" }
