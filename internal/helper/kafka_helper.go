package helper

import (
	"context"
	"fmt"
	"os"
	"strconv"
	"sync"
	"time"

	"github.com/segmentio/kafka-go"
)

var (
	kafkaWriterConnMgmt = make(map[string]*kafka.Writer)
	kafkaWriterConnMu   sync.RWMutex
)

func getKafkaWriterConn(addr string, topic string) *kafka.Writer {
	key := fmt.Sprintf("%s_%s", addr, topic)

	kafkaWriterConnMu.RLock()
	conn, ok := kafkaWriterConnMgmt[key]
	if ok && conn != nil {
		defer kafkaWriterConnMu.RUnlock()
		return conn
	}
	kafkaWriterConnMu.RUnlock()

	// user round-robin
	conn = &kafka.Writer{
		Addr:  kafka.TCP(addr),
		Topic: topic,
	}
	kafkaWriterConnMu.Lock()
	kafkaWriterConnMgmt[key] = conn
	kafkaWriterConnMu.Unlock()

	return conn
}

func sendMessage(mess string) {
	writer := getKafkaWriterConn(os.Getenv("KAFKA_ADDRESS"), os.Getenv("KAFKA_TOPIC_PAGEVIEW"))

	time := time.Now().UnixNano() / int64(time.Millisecond)
	timeString := strconv.FormatInt(time, 10)

	val := timeString + "\t" + mess

	fmt.Println(val)

	err := writer.WriteMessages(context.Background(), kafka.Message{
		Value: []byte(val),
	})

	if err != nil {
		fmt.Println(err)
	}
}

type kafkaWriter struct {
	writer    *kafka.Writer
	batchSize int
	messages  []kafka.Message
	mu        sync.Mutex
}

var singleInstance *kafkaWriter

var lock = &sync.Mutex{}

func GetInstance() *kafkaWriter {
	if singleInstance == nil {
		lock.Lock()
		defer lock.Unlock()
		if singleInstance == nil {
			fmt.Println("Creating single kk now.")
			singleInstance = newKafkaWriter()
		} else {
			// fmt.Println("Single kk already created.")
		}
	} else {
		// fmt.Println("Single kk already created.")
	}

	return singleInstance
}

func newKafkaWriter() *kafkaWriter {

	writer := getKafkaWriterConn(os.Getenv("KAFKA_ADDRESS"), os.Getenv("KAFKA_TOPIC_PAGEVIEW"))

	batchSize, _ := strconv.Atoi(os.Getenv("KAFKA_BATCH_SIZE"))
	kafkaWriter := &kafkaWriter{
		writer:    writer,
		batchSize: batchSize,
		messages:  make([]kafka.Message, 0, batchSize),
	}
	return kafkaWriter
}

func (w *kafkaWriter) SendMessage(mess string) {

	//TODO: check starvation
	w.mu.Lock()
	defer w.mu.Unlock()
	// store message
	time := time.Now().UnixNano() / int64(time.Millisecond)
	timeString := strconv.FormatInt(time, 10)

	val := timeString + "\t" + mess
	w.messages = append(w.messages, kafka.Message{
		Value: []byte(val),
	})

	lenMessage := len(w.messages)
	// check batched messages size
	if w.batchSize == lenMessage {
		// ref to batched messages
		batchedMessage := w.messages
		// update batched messages ref
		w.messages = make([]kafka.Message, 0, w.batchSize)
		// write batched messages to kafka
		go func() {
			err := w.writer.WriteMessages(context.Background(), batchedMessage...)
			if err != nil {
				// handle write kafka error
				fmt.Println("write batched message to kafka error %v", err)
			}
		}()

	}
}
