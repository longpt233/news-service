package repo_impl

import (
	"article-service/internal/domain/entity"
	"article-service/internal/infrastructure/database"
)

type ArticleMysql struct {
}

func (articleMysql *ArticleMysql) FindAll(filter map[string]interface{}, limit int, offset int, query string) (articles []*entity.ArticleEntity, err error) {
	conn := database.GetInstance()
	return articles, conn.Where(filter).Limit(limit).Offset(offset).Order(query).Find(&articles).Error
}

func (articleMysql *ArticleMysql) Count(filter map[string]interface{}, query string) (result int64) {
	conn := database.GetInstance()
	var count int64
	conn.Model(&entity.ArticleEntity{}).Where(filter).Order(query).Count(&count)
	return count
}
