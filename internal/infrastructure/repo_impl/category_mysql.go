package repo_impl

import (
	"article-service/internal/domain/entity"
	"article-service/internal/infrastructure/database"
)

type CategoryMysql struct {
}

func (cateMysql *CategoryMysql) GetAll() (cate []*entity.CategoryEntity, err error) {
	conn := database.GetInstance()
	return cate, conn.Model(&entity.CategoryEntity{}).Find(&cate).Error
}

func (cateMysql *CategoryMysql) GetOne(id uint) (cate *entity.CategoryEntity, err error) {
	conn := database.GetInstance()
	return cate, conn.First(&cate, id).Error
}
