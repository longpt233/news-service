package handler

import (
	"article-service/internal/domain/entity"
	"article-service/internal/infrastructure/model/response"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type CategoryService interface {
	GetAll() ([]*entity.CategoryEntity, error)
	GetOne(id uint) (*entity.CategoryEntity, error)
}

type categoryController struct {
	categoryService CategoryService
}

func NewCategoryController(us CategoryService) *categoryController {
	return &categoryController{us}
}

func (nc *categoryController) GetAll(c *gin.Context) {

	cate, err := nc.categoryService.GetAll()
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}

	categoryResponses := response.ListCategoryEntityToResponse(cate)

	data := map[string]interface{}{
		"categories": categoryResponses,
	}
	c.JSON(http.StatusOK, response.BuildSuccessResponse(data))
}

func (nc *categoryController) GetOne(c *gin.Context) {

	// Param is /:id
	// Query is ?id=
	idCategory, err := strconv.Atoi((c.Param("id")))
	if err != nil || idCategory < 0 {
		c.JSON(http.StatusBadRequest, "id must be uint")
		return
	}

	cate, err := nc.categoryService.GetOne(uint(idCategory))
	if err != nil {
		c.JSON(http.StatusInternalServerError, "record not found")
		return
	}

	categoryResponses := response.CategoryEntityToResponse(cate)

	data := map[string]interface{}{
		"category": categoryResponses,
	}
	c.JSON(http.StatusOK, response.BuildSuccessResponse(data))
}
