package handler

import (
	"article-service/internal/domain/entity"
	"article-service/internal/helper"
	"article-service/internal/infrastructure/model/response"
	"fmt"
	"math"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type ArticleService interface {
	Get(filter map[string]interface{}, limit int, offset int, query string) ([]*entity.ArticleEntity, error)
	Count(filter map[string]interface{}, query string) (uint, error)
}

type articleController struct {
	articleService ArticleService
}

func NewArticleController(us ArticleService) *articleController {
	return &articleController{us}
}

func (nc *articleController) GetArticle(ctx *gin.Context) {

	sortBy := ctx.Query("sort_by")
	if sortBy == "" {
		sortBy = "created_at.desc" // sortBy is expected to look like field.orderdirection i. e. id.asc
	}
	sortQuery, err := helper.ValidateAndReturnSortQuery(entity.ArticleEntity{}, sortBy)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, response.BuildResponse(-1, "invalid param sort", err.Error()))
		return
	}

	// tao query limit
	strLimit := ctx.Query("limit")
	limit := -1 // with a value as -1 for gorms Limit method, we'll get a request without limit as default
	if strLimit != "" {
		limit, err = strconv.Atoi(strLimit)
		if err != nil || limit < -1 {
			ctx.JSON(http.StatusBadRequest, response.BuildResponse(-1, "limit query parameter is no valid number", err.Error()))
			return
		}
	}

	// tạo query offset
	strOffset := ctx.Query("offset")
	offset := 1
	if strOffset != "" {
		offset, err = strconv.Atoi(strOffset)
		if err != nil || offset < -1 {
			ctx.JSON(http.StatusBadRequest, response.BuildResponse(-1, "offset query parameter is no valid number", err.Error()))
			return
		}
	}
	offsetByPage := (offset - 1) * limit // offset trên query tính từ 1

	// tạo query filter
	filter := ctx.Query("filter")
	filterMap := map[string]interface{}{}
	if filter != "" {
		filterMap, err = helper.ValidateAndReturnFilterMap(entity.ArticleEntity{}, filter)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, response.BuildResponse(-1, "invalid filter param ", err.Error()))
			return
		}
	}

	// fmt.Println("query:", filterMap, limit, offsetByPage, sortQuery)
	total, err := nc.articleService.Count(filterMap, sortQuery)

	article, err := nc.articleService.Get(filterMap, limit, offsetByPage, sortQuery)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err)
		return
	}

	userId := ctx.Query("user_id")
	if userId == "" {
		userId = "-1" // khong co user thi la -1
	}

	kkWriter := helper.GetInstance() //mỗi req đến lại tạo mới 1 instance

	extra := ctx.Query("extra")
	if extra == "" {
		extra = "{}" // khong co thông tin thì là -1, tránh nỗi sau này parse \t sai
	}

	if val, hasKey := filterMap["id"]; hasKey {
		mess := userId + "\t2\t" + fmt.Sprint(val) + "\t" + extra // 2 là lấy cụ thể 1 bài viết (đọc bài)
		kkWriter.SendMessage(mess)
	} else {

		for _, iter := range article {
			mess := userId + "\t14\t" + strconv.FormatUint(uint64(iter.ID), 10) + "\t" + extra // 14 là đã lướt qua bài viết(các bài viết đã được hiển thị cho người dùng)
			kkWriter.SendMessage(mess)
		}

	}

	articleResponses := response.ListArticleEntityToResponse(article)

	data := map[string]interface{}{
		"articles":       articleResponses,
		"total_page":     math.Ceil(float64(total) / float64(limit)),
		"offset_page":    offset,
		"limit_page":     limit,
		"total_articles": total,
	}
	ctx.JSON(http.StatusOK, response.BuildSuccessResponse(data))
}
