package response

import (
	"article-service/internal/domain/entity"
	"strconv"
	"time"
)

type ArticleResponse struct {
	Title            string    `json:"title"`
	Content          string    `json:"content"`
	CategoryEntityId uint      `json:"category_entity_id"`
	CreatedAt        time.Time `json:"created_at"`
	Id               string    `json:"article_id"`
}

type Response struct {
	StatusCode int         `json:"status"`
	Message    string      `json:"message"`
	Data       interface{} `json:"data"`
}

func BuildResponse(status int, message string, data interface{}) Response {
	res := Response{
		StatusCode: status,
		Message:    message,
		Data:       data,
	}
	return res
}

func BuildSuccessResponse(obj interface{}) Response {

	return BuildResponse(200, "successfuly", obj)
}

func ArticleEntityToResponse(article *entity.ArticleEntity) ArticleResponse {
	return ArticleResponse{
		Title:            article.Title,
		Content:          article.Content,
		CategoryEntityId: article.CategoryEntityId,
		CreatedAt:        article.CreatedAt,
		Id:               strconv.Itoa(int(article.ID)),
	}
}

func ListArticleEntityToResponse(articles []*entity.ArticleEntity) []*ArticleResponse {

	res := make([]*ArticleResponse, 0, len(articles)) // thêm 0 vào đây nếu không nó sẽ trẻ về 1 slices [nil]

	for _, articleIter := range articles {
		one := ArticleEntityToResponse(articleIter)
		res = append(res, &one)
	}

	return res
}

type CategoryResponse struct {
	ID   uint   `json:"category_id"`
	Name string `json:"category_name"`
}

func CategoryEntityToResponse(cate *entity.CategoryEntity) CategoryResponse {
	return CategoryResponse{
		cate.ID,
		cate.Name,
	}
}

func ListCategoryEntityToResponse(cate []*entity.CategoryEntity) []*CategoryResponse {

	res := make([]*CategoryResponse, 0, len(cate))

	for _, cateIter := range cate {
		cateResponse := CategoryEntityToResponse(cateIter)
		res = append(res, &cateResponse)
	}

	return res
}
