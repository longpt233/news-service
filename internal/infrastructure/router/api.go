package router

import (
	"net/http"
	"os"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

type SubRouter interface {
	Apply(router *gin.RouterGroup)
}

func RunAPI() error {

	r := gin.Default()

	config := cors.DefaultConfig()
	config.AllowAllOrigins = true
	config.AllowHeaders = []string{"Origin", "Content-Length", "Content-Type", "Authorization"}
	r.Use(cors.New(config))

	r.GET("/news/ping", func(ctx *gin.Context) {
		ctx.String(http.StatusOK, "pong")
	})

	article := r.Group("/news/api/v1")

	var newsRouter SubRouter = articleRouter{}
	newsRouter.Apply(article)

	add := os.Getenv("APP_ADDRESS")

	return r.Run(add)
}
