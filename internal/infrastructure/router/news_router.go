package router

import (
	"article-service/internal/application"
	"article-service/internal/infrastructure/handler"
	"article-service/internal/infrastructure/repo_impl"

	"github.com/gin-gonic/gin"
)

type articleRouter struct {
}

func (router articleRouter) Apply(r *gin.RouterGroup) {

	repo := repo_impl.ArticleMysql{}
	serviceImp := application.NewArticleService(&repo)
	// interface phải truyền địa chỉ khi method là pointer receiver
	// https://200lab.io/blog/interface-trong-golang-cach-dung-chinh-xac/
	var articleHandler = handler.NewArticleController(serviceImp)
	r.GET("/articles", articleHandler.GetArticle)

	repoCate := repo_impl.CategoryMysql{}
	serviceCateImp := application.NewCategoryService(&repoCate)
	var controller = handler.NewCategoryController(serviceCateImp)
	r.GET("/categories", controller.GetAll)
	r.GET("/categories/:id", controller.GetOne)
}
