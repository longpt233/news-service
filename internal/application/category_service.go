package application

import "article-service/internal/domain/entity"

type CategoryRepository interface {
	GetAll() ([]*entity.CategoryEntity, error)
	GetOne(id uint) (*entity.CategoryEntity, error)
}

type categoryService struct {
	repo CategoryRepository
}

func NewCategoryService(r CategoryRepository) *categoryService {
	return &categoryService{r}
}

func (service *categoryService) GetAll() ([]*entity.CategoryEntity, error) {
	return service.repo.GetAll()
}

func (service *categoryService) GetOne(id uint) (*entity.CategoryEntity, error) {
	return service.repo.GetOne(id)
}
