package application

import "article-service/internal/domain/entity"

// dùng ở đâu khai báo interface ở đấy -> tránh cycle import, cha không phụ thuộc vào con.
type ArticleRepository interface {
	FindAll(filter map[string]interface{}, limit int, offset int, query string) (article []*entity.ArticleEntity, err error)
	Count(filter map[string]interface{}, query string) (result int64)
}

type articleService struct {
	repo ArticleRepository
}

func NewArticleService(r ArticleRepository) *articleService {
	return &articleService{r}
}

func (service *articleService) Get(filter map[string]interface{}, limit int, offset int, query string) ([]*entity.ArticleEntity, error) {

	news, err := service.repo.FindAll(filter, limit, offset, query)
	if err != nil {
		return nil, err
	}

	return news, nil

}

func (service *articleService) Count(filter map[string]interface{}, query string) (uint, error) {
	return uint(service.repo.Count(filter, query)), nil
}
